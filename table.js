const action = document.getElementById('action')
const map = document.getElementById('map')
const tableData = document.getElementById('tableData')

for (let i = 0; i < localStorage.length; i++) {
     const key = localStorage.key(i);
     const value = JSON.parse(localStorage.getItem(key));
     tableData.innerHTML += 
     `<tbody>
          <tr colspan="2">
               <th colspan="2"> ${value.userName} </th>
               <th colspan="2"> ${value.phone} </th>
                        <th>
                         <details>
                                   <summary></summary>
                              <span>
                                   <div class="mapouter">
                                   <div class="map">
                                        <iframe src="http://maps.google.com/maps?q=${value.address}&z=16&output=embed" height="250" width="250"></iframe>
                                   </div>
                                    </div>
                                        ${value.notes}
                              </span>
                         </details>
                         </th>
          </tr>
     </tbody>`
}